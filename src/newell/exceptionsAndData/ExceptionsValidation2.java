package newell.exceptionsAndData;

import java.util.Scanner;

class ExceptionsValidation2 {

    /*create a program that gathers input of two numbers from the
     * keyboard in its main
     * method and then calls the method below to calculate */
    public static void main(String[] args) {
        System.out.println("What is the first number?      ");
        Scanner in = new Scanner(System.in);

        // accept new string input and parse into int
        String string1 = in.nextLine();
        int num1 = Integer.parseInt(string1);
        System.out.println("The first number is: " + num1);

        /*
        * accept second string input and parse into int
        * should ask for new input if a value of 0 is entered
        */
        int num2 = 0;

        do {
            System.out.println("What is the second number?     ");

            String string2 = in.nextLine();
            num2 = Integer.parseInt(string2);
            System.out.println("The second number is : " + num2);
            if (num2 == 0) {
                System.out.println("Dividing by zero is impossible, please select another number: ");

            } else
                {
                    continue;
                }
        } while (num2 == 0);

        // call doDivision method and accept quotient
        int quotient = doDivision(num1, num2);

        System.out.println("You have chosen: "+ num1 + "/" + num2);
        System.out.println("The quotient is: " + quotient);

    }

    /* per 2e-g, rewrite the data collection so that it uses data validation to ensure
    * an error does not occur.
    * doDivision should not throw an exception in this instance
     */
    public static int doDivision(int a, int b) {
        int result = a/b;
        return result;
    }
}



