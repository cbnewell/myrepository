package newell.exceptionsAndData;

import java.util.Scanner;

class ExceptionsValidation {

    /*create a program that gathers input of two numbers from the
     * keyboard in its main
     * method and then calls the method below to calculate */
    public static void main(String[] args) {
        System.out.println("What is the first number?      ");
        Scanner in = new Scanner(System.in);
        String string1;
        String string2;

        // accept new string input and parse into int
        string1 = in.nextLine();
        int num1 = Integer.parseInt(string1);
        System.out.println("The first number is:   " + num1);

        // accept second string input and parse into int
        System.out.println("What is the second number?     ");
        string2 = in.nextLine();
        int num2 = Integer.parseInt(string2);
        System.out.println("The second number is:   " + num2);

        // call doDivision method and accept quotient
        int quotient = doDivision(num1, num2);

        System.out.println("The quotient is:   " + quotient);



    }
    public static int doDivision(int a, int b) {
        int result = 0;
        Scanner in2 = new Scanner(System.in);
        // if second int is 0, throw exception
        while (b==0) {

                try {
                    System.out.println("You have chosen:   "+ a + "/" + b);

                    result = a / b;
                } catch (ArithmeticException e)   {
                    System.out.println("Dividing by zero is impossible... Please choose another number: ");

                    String stringb = in2.nextLine();
                    b = Integer.parseInt(stringb);

                }

        }
        System.out.println("You have chosen:   "+ a + "/" + b);
         result = a/b;
        return  result;
    }
}



