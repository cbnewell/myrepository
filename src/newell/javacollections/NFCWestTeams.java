package newell.javacollections;


public class NFCWestTeams {

    private String teamname;
    private String quarterback;
    public int qbNumber;

    //constructor
    public NFCWestTeams(String teamname, String quarterback, int qbNumber)
    {
        this.teamname = teamname;
        this.quarterback = quarterback;
        this.qbNumber = qbNumber;


    }
    // prints team details in Main()
    public String toString()
    {
        return "Team Name: " + teamname + "... Quarterback: " + quarterback;
    }
}
