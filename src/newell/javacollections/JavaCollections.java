package newell.javacollections;

import java.util.*;



public class JavaCollections {

    public static void main(String[] args) {


        // creates a Linked Hash Set for the NFCWestTeams class
        System.out.println("--The following is a Generics Set of NFC West Football Teams in alphabetical order");
        Set<NFCWestTeams> NFCWest = new LinkedHashSet<NFCWestTeams>();
        NFCWest.add(new NFCWestTeams( "Seattle Seahawks", "Russell Wilson", 3));
        NFCWest.add(new NFCWestTeams("Arizona Cardinals", "Kyler Murray", 1));
        NFCWest.add(new NFCWestTeams("Los Angeles Rams", "Jared Goff", 16));
        NFCWest.add(new NFCWestTeams("San Francisco 49ers", "Jimmy Garoppolo", 10));
        // prints out teams in NFCWest Set
        for (NFCWestTeams team : NFCWest)
        {
            System.out.println(team);
        }

        System.out.println();

        // creates an Array List of the NFC West Teams, notable duplicates to show features
        System.out.println("--The following is a list of the best NFC West Teams in order:");
        List BestWest = new ArrayList();
        BestWest.add("Seattle Seahawks");
        BestWest.add("Seattle Seahawks");
        BestWest.add("Arizona Cardinals");
        BestWest.add("Los Angeles Rams");
        BestWest.add("Seattle Seahawks Practice Squad");
        BestWest.add("San Francisco 49ers");

        // print out list contents
        for (Object str : BestWest) {
            System.out.println((String)str);
        }

        System.out.println();

        // to showcase the queue features, create a queue and then remove and add a new team...
        // should show that items are replaced in same location as object removed
        System.out.println("--The following is an incorrect list of NFC West Teams:");
        Queue WrongWest = new PriorityQueue();

        WrongWest.add("Dallas Cowboys");
        WrongWest.add("Seattle Seahawks");
        WrongWest.add("Los Angeles Rams");
        WrongWest.add("Arizona Cardinals");

        // print wrong items
        Iterator iterator = WrongWest.iterator();
        while (iterator.hasNext())  {
            System.out.println(iterator.next());

        }

        System.out.println();

        System.out.println("----Whoops Not Dallas!!!" + "\r\n");

        // remove cowboys, add 49ers
        WrongWest.remove("Dallas Cowboys");
        WrongWest.add("San Francisco 49ers");

        // use while loop and iterators as shown in example to print correct items
        Iterator iterator1 = WrongWest.iterator();
        while (iterator1.hasNext())  {
            System.out.println(iterator1.next());
        }

        System.out.println();

        System.out.println("----That's Better" + "\r\n");

        System.out.println("--Now a list of the all time coolest NFC West Football teams with no bias:");
        // showcase TreeMap Collection, shows items in order being replaced as necessary
        Map WildWestMap = new TreeMap();
        WildWestMap.put(1,"Seattle Seahawks");
        WildWestMap.put(4,"San Fransisco 49ers");
        WildWestMap.put(3,"Los Angeles Rams");
        WildWestMap.put(2,"Arizona Cardinals");
        WildWestMap.put(4,"Seattle Seahawks Practice Squad");
        WildWestMap.put(5,"San Francisco 49ers");


        for (int i = 1; i < 6; i++ )
        {
            String WildWestOrder = (String)WildWestMap.get(i);
            System.out.println(WildWestOrder);
        }

        System.out.println();


    }
}
