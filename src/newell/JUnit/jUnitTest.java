package newell.JUnit;


import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.*;

public class jUnitTest {


    @Test
        public void testStadiumName()
    {
        try {
            Scanner scanner = new Scanner(System.in);
            String stadiumA = "Century Link Field";
            System.out.println("What is the name of the Seahawks Stadium? ");

            String insideString = scanner.nextLine();
            String stadiumB = insideString;
            // assertSame to  show that both stadium fields are the same object, not just equal in value
            assertEquals(stadiumA, stadiumB);

        } catch (Exception e){
            System.err.println(e.toString());
        }



    }

    @Test
        public void testQBAge()
    {
        int rWilson = 31;
        int gSmith = 30;
        // Check if Russell Wilson is older than Geno Smith
        assertTrue(rWilson > gSmith);
        // check if Geno Smith is older than Russell Wilson
        assertFalse(rWilson < gSmith);
    }

    @Test
        public void testTopThreeWR()
    {
        /*
        String[] topThreeA = {"David Moore", "Tyler Lockett", "D.K. Metcalf"};
        String[] topThreeB = {"Tyler Lockett", "David Moore", "D.K. Metcalf"};
        // check if the two arrays are equivalent in values and organization
        assertArrayEquals(topThreeA, topThreeB);
        */
        // try again with correct values
        String[] topThreeC = {"D.K. Metcalf", "Tyler Lockett", "David Moore"};
        String[] topThreeD = {"D.K. Metcalf", "Tyler Lockett", "David Moore"};
        // check if the two arrays are equivalent in values and organization
        assertArrayEquals(topThreeC, topThreeD);
    }

    @Test
        public void testBestFootballTeamsInAmerica()
    {
        String Team1 = null;
        String Team2 = "Seattle Seahawks";
        // test to see if Team1 is null
        assertNull(Team1);
        // test to show Team2 is populated/not null
        assertNotNull(Team2);
    }

    @Test
        public void testTeamColors()
    {
        String color1 = "Blue";
        String color2 = "Green";
        // assert that color 1 and color 2 are not the same
        assertNotSame(color1,color2);

    }
    @Test
        public void testSuperBowlWins()
    {
        int SuperbowlWins = 1;
        int SuperbowlLosses = 1;
        // assert that losses and wins are the the same using assertSame
        assertSame(SuperbowlWins, SuperbowlLosses);
    }

}
