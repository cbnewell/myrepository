package newell.JUnit;

import org.junit.runner.Result;
import org.junit.runner.JUnitCore;
import org.junit.runner.notification.Failure;


public class FootballTests {

    public static void main(String[] args) {

        Result result = JUnitCore.runClasses(jUnitTest.class);

        try {
            for(Failure failure : result.getFailures()){
                System.out.println(failure.toString());
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}
