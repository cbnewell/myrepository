package newell.HTTPAndJSON;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;


//The second component is a client component.  It makes a call to the server component, gets the JSON it returns, and then converts that using the Jackson JSON parser, into an object.

public class JSONtoJavaObject {

    public static JsonObject JSONToJava(File s) {

        ObjectMapper mapper = new ObjectMapper();
        JsonObject json = null;
        try{
            json = mapper.readValue(Paths.get("JsonFile.json").toFile(), JsonObject.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    public static void main(String[] args) {

        File f = new File("src/newell/HTTPAndJSON/JsonFile1.json");

        JsonObject jsonObject = JSONToJava(f);

        System.out.println(jsonObject);



    }
}
