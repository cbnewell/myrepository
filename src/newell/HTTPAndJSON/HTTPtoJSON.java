package newell.HTTPAndJSON;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;


//The first is an HTTP server component that when called takes an object and converts it to JSON using the Jackson JSON parser, and returns a block of JSON using HTTP.

public class HTTPtoJSON {


    // get object data from HTTP in order and convert it to JSON using JSON parser, returning a block of JSON
    public static String getHttpContent(String string) {

        String content = "";


        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line;
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            content = stringBuilder.toString();



        } catch (Exception e) {
            System.err.println(e.toString());
        }


        return content;

    }

    public static void main(String[] args) {

        // create JSON obj
        ObjectMapper mapper = new ObjectMapper();
        String s = HTTPtoJSON.getHttpContent("http://www.google.com");
        File f = new File("src/newell/HTTPAndJSON/JsonFile1.json");
        // java object to new JSON file
        try {
            mapper.writeValue(f, s);

            String jsonString = mapper.writeValueAsString(s);
            System.out.println(jsonString);

        } catch (IOException e) {
            System.err.println(e.toString());
        }
    }
}
