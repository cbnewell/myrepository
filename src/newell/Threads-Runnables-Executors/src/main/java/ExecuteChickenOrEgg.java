
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class ExecuteChickenOrEgg {
    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(2);

        System.out.println("Which do you think comes first? Chicken or the egg?");
        ChickenOrEgg Ex1 = new ChickenOrEgg("the Chicken");
        ChickenOrEgg Ex2 = new ChickenOrEgg("the Egg");

        myService.execute(Ex1);
        myService.execute(Ex2);


        myService.shutdown();





    }
}