package newell.W7.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet1", urlPatterns ={"/Servlet1"})
public class Servlet1 extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        out.println("<html><head><title>Join The Squad!</title></head><body>");

        String position = request.getParameter("position");
        String number = request.getParameter("number");

        out.println("<h1>If you were playing for the Seattle Seahawks you would want to be a: </h1>");
        out.println("<p>" + position + "</p>");
        out.println("<p>Wearing the number " + number + "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<h1>You tried to invoke the Java Servlet. This has summoned a great evil. What were you thinking???</h1>");
    }
}
